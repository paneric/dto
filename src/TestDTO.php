<?php

declare(strict_types=1);

namespace Paneric\DTO;

class TestDTO extends DTO
{
    protected $userId;
    protected $userRef;

    public function getUserId()//without typing, waiting for union
    {
        return $this->userId;
    }
    public function getUserRef()
    {
        return $this->userRef;
    }

    protected function setUserId($userId): void//without typing, waiting for union
    {
        $this->userId = is_array($userId) ?
            $userId :
            (int) $userId;
    }
    protected function setUserRef($userRef): void
    {
        $this->userRef = $userRef;
    }
}
